﻿/*
 * Program ID: Assignment 1 Part 2
 * 
 * Purpose: To have console display info about me
 * 
 * Revision History:
 *      Written Sept 2017 by Mason Spencer
 *      
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Assignment 1 Part 2");
            Console.WriteLine("Name: Mason Spencer");
            Console.WriteLine("Email: Mspencer3538@conestogac.on.ca");
            Console.WriteLine("Program: CPA");
            Console.WriteLine("Experience: I have no previous " +
                "programming experience");
            Console.ReadLine();
        }
    }
}
