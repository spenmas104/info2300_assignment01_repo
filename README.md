Author: Mason Spencer

This is the README for the INFO2300_Assignment01 Repository.
This Repo was created for Individual Assignment 1 For my INFO2300 course.

To buid and install this project please download the files.
Once the files are downloaded, open the file and run the .sln file in either Visual Studio or open the project folder in VS Code.
Once you have it opened in the code editor click the run button on visual studio or type dotnet run in VS Code. 
It will then be automaticaly built and open.
 
I chose the MIT license for this repo,
I chose it because it was a very generic license with little restriction or complications. 